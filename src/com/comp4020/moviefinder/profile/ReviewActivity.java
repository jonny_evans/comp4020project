package com.comp4020.moviefinder.profile;

import android.content.Intent;
import android.os.Bundle;
import android.app.Activity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.AdapterView.OnItemClickListener;

import com.comp4020.moviefinder.DetailViewActivity;
import com.comp4020.moviefinder.R;
import com.comp4020.moviefinder.data.*;

public class ReviewActivity extends Activity implements OnItemClickListener{
	
	User user;
	Review[] reviewList;
	
	public void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
        setContentView(R.layout.review_profile_layout);
        
        Intent i = this.getIntent();
        Bundle intentBundle = i.getExtras();
        String userID = intentBundle.getString("userID");
        user = UserDataLayer.getUserDataLayer().getUserById(userID);
        
        ListView reviewList = (ListView)this.findViewById(R.id.listReviews);
        String[] reviewStr = new String[user.reviews.size()];
        ReviewArrayAdapter arrayAdapter = new ReviewArrayAdapter(this,reviewStr);
                
        reviewList.setAdapter(arrayAdapter);
        
        if(user.moviesToWatch != null)
        {
        	this.reviewList = user.reviews.values().toArray(new Review[0]);
        	arrayAdapter.setReviews(this.reviewList);
        	reviewList.setOnItemClickListener(this);
        }
    }
	
	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position, long id)
    {
        Intent i = new Intent(this, DetailViewActivity.class);        
        String movieID = this.reviewList[position].movieID;
		i.putExtra("movieID", movieID);
		this.startActivity(i);
    } 
}
