package com.comp4020.moviefinder.profile;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

import com.comp4020.moviefinder.DetailViewActivity;
import com.comp4020.moviefinder.R;
import com.comp4020.moviefinder.data.User;
import com.comp4020.moviefinder.data.UserDataLayer;

public class WatchActivity extends Activity implements OnItemClickListener{
	
	private User user;
	
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
        setContentView(R.layout.watch_profile_layout);
        
        Intent i = this.getIntent();
        
        Bundle intentBundle = i.getExtras();
        String userID = intentBundle.getString("userID");
        user = UserDataLayer.getUserDataLayer().getUserById(userID);
        
        ListView watchList = (ListView)this.findViewById(R.id.listWatch);
        
        if(user.moviesToWatch != null)
        {
            WatchArrayAdapter arrayAdapter = new WatchArrayAdapter(this, user.moviesToWatch);
        	watchList.setAdapter(arrayAdapter);
        	watchList.setOnItemClickListener(this);
        }
    }

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position, long id)
    {
        Intent i = new Intent(this, DetailViewActivity.class);
		String movieID = "";
		movieID = user.moviesToWatch.get(position);
		
		i.putExtra("movieID", movieID);
		this.startActivity(i);
    } 
}
