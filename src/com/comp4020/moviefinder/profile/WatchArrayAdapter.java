package com.comp4020.moviefinder.profile;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.comp4020.moviefinder.R;
import com.comp4020.moviefinder.data.*;


public class WatchArrayAdapter extends BaseAdapter {
	private final Context context;
	//private final String[] values;
	private List<String> values;

	public WatchArrayAdapter(Context context, List<String> values) {
		this.context = context;
		this.values = values;
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		LayoutInflater inflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View rowView = inflater.inflate(R.layout.watch_item, parent, false);
		TextView textTitleView = (TextView) rowView.findViewById(R.id.textTitle);
		
		textTitleView.setText(MovieDataLayer.getMovieDataLayer().getMovieById(this.values.get(position)).title);
		
		return rowView;
	}

	@Override
	public int getCount() {
		return this.values.size();
	}

	@Override
	public Object getItem(int position) {
		return this.values.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}
}
