package com.comp4020.moviefinder.profile;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.comp4020.moviefinder.R;
import com.comp4020.moviefinder.data.*;


public class ReviewArrayAdapter extends ArrayAdapter<String> {
	private final Context context;
	private final String[] values;
	
	private Review[] reviews;
	
	//private List<Review> reviews;

	public ReviewArrayAdapter(Context context, String[] values) {
		super(context, R.layout.review_item, values);
		this.context = context;
		this.values = values;
	}
	
	public void setReviews(Review[] reviews)
	{
		this.reviews = reviews;
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		LayoutInflater inflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View rowView = inflater.inflate(R.layout.review_item, parent, false);
		TextView textTitleView = (TextView) rowView.findViewById(R.id.textTitle);
		TextView textReviewView = (TextView) rowView.findViewById(R.id.textReview);
		TextView textRatingView = (TextView) rowView.findViewById(R.id.textRating);
		String temp = "";
		
		temp = reviews[position].movieID;
		//temp = reviews.get(position).movieID;
		
		textTitleView.setText(MovieDataLayer.getMovieDataLayer().getMovieById(temp).title);
		textReviewView.setText(reviews[position].review);
		textRatingView.setText(String.valueOf(reviews[position].rating)+" out of 10");
		
		return rowView;
	}
}
