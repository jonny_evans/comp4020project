package com.comp4020.moviefinder.profile;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.comp4020.moviefinder.R;
import com.comp4020.moviefinder.data.*;

public class SuggestArrayAdapter  extends ArrayAdapter<String> {
	private final Context context;
	private final String[] values;
	private List<Suggested> suggestedList;

	public SuggestArrayAdapter(Context context, String[] values) {
		super(context, R.layout.suggested_item, values);
		this.context = context;
		this.values = values;
	}
	
	public void setSuggested(List<Suggested> suggestedList)
	{
		this.suggestedList = suggestedList;
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		LayoutInflater inflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View rowView = inflater.inflate(R.layout.suggested_item, parent, false);
		TextView textTitleView = (TextView) rowView.findViewById(R.id.textTitle);
		TextView textUserView = (TextView) rowView.findViewById(R.id.textUser);
		String temp = "";
		
		temp = suggestedList.get(position).movieID;
		textTitleView.setText(MovieDataLayer.getMovieDataLayer().getMovieById(temp).title);
		
		temp = suggestedList.get(position).userID;
		textUserView.setText(UserDataLayer.getUserDataLayer().getUserById(temp).name);
		
		return rowView;
	}
}
