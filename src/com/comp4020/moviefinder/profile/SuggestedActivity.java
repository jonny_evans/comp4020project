package com.comp4020.moviefinder.profile;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

import com.comp4020.moviefinder.DetailViewActivity;
import com.comp4020.moviefinder.R;
import com.comp4020.moviefinder.data.MovieDataLayer;
import com.comp4020.moviefinder.data.User;
import com.comp4020.moviefinder.data.UserDataLayer;

public class SuggestedActivity extends Activity implements OnItemClickListener{
	
	private User user;
	
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
        setContentView(R.layout.suggested_profile_layout);
        Intent i = this.getIntent();
        
        Bundle intentBundle = i.getExtras();
        String userID = intentBundle.getString("userID");
        user = UserDataLayer.getUserDataLayer().getUserById(userID);
        
        ListView suggestList = (ListView)this.findViewById(R.id.listSuggest);
        String[] suggestStr = new String[user.suggestedMovies.size()];
        SuggestArrayAdapter arrayAdapter = new SuggestArrayAdapter(this,suggestStr);
        
        if(user.suggestedMovies != null)
        {
        	Log.d("MovieFinder","Suggested Movie List Size:\t"+user.suggestedMovies.size());
        	if(user.suggestedMovies.size() == 2)
        	Log.d("MovieFinder","Suggested Movie List Movie:\t"+UserDataLayer.getUserDataLayer().getUserById(user.suggestedMovies.get(1).userID).name);
        	arrayAdapter.setSuggested(user.suggestedMovies);
        
        	suggestList.setAdapter(arrayAdapter);
        	
        	suggestList.setOnItemClickListener(this);
        }
        
    }

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position, long id)
    {
        Intent i = new Intent(this, DetailViewActivity.class);
		String movieID = "";
		movieID = user.suggestedMovies.get(position).movieID;
		
		i.putExtra("movieID", movieID);
		this.startActivity(i);
    } 
}
