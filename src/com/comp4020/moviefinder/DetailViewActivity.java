package com.comp4020.moviefinder;

import java.util.List;

import com.comp4020.moviefinder.data.Movie;
import com.comp4020.moviefinder.data.MovieDataLayer;
import com.comp4020.moviefinder.data.Query;
import com.comp4020.moviefinder.data.Review;
import com.comp4020.moviefinder.data.Suggested;
import com.comp4020.moviefinder.data.User;
import com.comp4020.moviefinder.data.UserDataLayer;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.PopupWindow;
import android.widget.TextView;

public class DetailViewActivity extends Activity implements OnClickListener
{
	private PopupWindow reviewPopup;
	private PopupWindow sharePopup;
	private PopupWindow watchPopup;
	
	private Button reviewBtn;
	private Button cancelRevBtn;
	private Button saveBtn;
	private Button shareBtn;
	private Button shareShareBtn;
	private Button shareCancelBtn;
	private Button watchBtn;
	private Button watchOkBtn;
	
	private User currentUser;
	private String movieID;
	
    public void onCreate(Bundle savedInstanceState) 
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.detail_view);
        
        Intent i = this.getIntent();
        
        Bundle intentBundle = i.getExtras();
        this.movieID = intentBundle.getString("movieID");
        Movie movie = MovieDataLayer.getMovieDataLayer().getMovieById(movieID);
        this.currentUser = UserDataLayer.getUserDataLayer().getCurrentUser();
        
        reviewBtn = (Button)this.findViewById(R.id.buttonReview);
        shareBtn = (Button)this.findViewById(R.id.buttonShare);
        watchBtn = (Button)this.findViewById(R.id.buttonWatch);
        
        // TextViews for Movie Info
        TextView txtTitle = (TextView)this.findViewById(R.id.textTitle);
        TextView txtLength = (TextView)this.findViewById(R.id.textLength);
        TextView txtYear = (TextView)this.findViewById(R.id.textYear);
        TextView txtGenre = (TextView)this.findViewById(R.id.textGenre);
        TextView txtDirect = (TextView)this.findViewById(R.id.textDirect);
        TextView txtAct = (TextView)this.findViewById(R.id.textAct);
        TextView txtSynopsis = (TextView)this.findViewById(R.id.textSynopsis);
        TextView txtCert = (TextView)this.findViewById(R.id.textCert);
        
        txtTitle.setText(movie.title);
        txtLength.setText(String.valueOf(movie.length+" min"));
        txtYear.setText(String.valueOf(movie.year));
        txtGenre.setText(movie.getGenre());
        txtDirect.setText(movie.director);
        txtAct.setText(movie.getActors());
        txtSynopsis.setText("<Synopsis Goes Here>");
        if(movie.certification != null)
        {
        	txtCert.setText(movie.certification);
    	}
        else
        {
        	txtCert.setText("Not Rated");
        }
        
        if(this.currentUser.moviesToWatch.contains(movieID))
        {
        	watchBtn.setText("Remove from List");
        }
        if(this.currentUser.reviews.containsKey(movieID))
        {
        	reviewBtn.setText("Edit Review");
        }
        
        reviewBtn.setOnClickListener(this);
        shareBtn.setOnClickListener(this);
        watchBtn.setOnClickListener(this);
    }                                     

	@Override
	public void onClick(View v) {
		LayoutInflater inflater = (LayoutInflater)this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View parent = this.findViewById(R.id.LinearLayout2);
		
		if(v == reviewBtn)
		{
			reviewPopup = new PopupWindow(inflater.inflate(R.layout.popup_review, null, false), parent.getWidth()-200, parent.getHeight()-200, true);
			reviewPopup.showAtLocation(parent, Gravity.CENTER, 0, 0);		
			
			EditText reviewText = (EditText)reviewPopup.getContentView().findViewById(R.id.textAreaReview);
			EditText ratingText = (EditText)reviewPopup.getContentView().findViewById(R.id.textAreaRating);
			
			Review review = this.currentUser.reviews.get(this.movieID);
			if(review != null)
			{
				reviewText.setText(review.review);
				ratingText.setText(String.valueOf(review.rating));
			}
			
			cancelRevBtn = (Button) reviewPopup.getContentView().findViewById(R.id.buttonReviewCancel);
			cancelRevBtn.setOnClickListener(this);
			saveBtn = (Button) reviewPopup.getContentView().findViewById(R.id.buttonSave);
			saveBtn.setOnClickListener(this);
		}
		else if(v == cancelRevBtn)
		{
			//cancel the review popup
			reviewPopup.dismiss();
		}
		else if(v == saveBtn)
		{
			//save review to this user's review list
			
			EditText reviewText = (EditText)reviewPopup.getContentView().findViewById(R.id.textAreaReview);
			EditText ratingText = (EditText)reviewPopup.getContentView().findViewById(R.id.textAreaRating);
			
			Review review = this.currentUser.reviews.get(this.movieID);
			if(review == null)
				review = new Review();
			
			review.movieID = this.movieID;
			review.review = reviewText.getText().toString();
			try
			{
				review.rating = Integer.parseInt(ratingText.getText().toString());			
			}
			catch(NumberFormatException nfe){
			}
			
			this.currentUser.reviews.put(this.movieID, review);
			
			reviewPopup.dismiss();
		}
		else if(v == shareBtn)
		{
			//share button
			sharePopup = new PopupWindow(inflater.inflate(R.layout.popup_share, null, false), parent.getWidth()-200, parent.getHeight()-200, true);
			sharePopup.showAtLocation(parent, Gravity.CENTER, 0, 0);
			
			shareShareBtn = (Button) sharePopup.getContentView().findViewById(R.id.buttonShareShare);
			shareShareBtn.setOnClickListener(this);
			
			shareCancelBtn = (Button) sharePopup.getContentView().findViewById(R.id.buttonShareCancel);
			shareCancelBtn.setOnClickListener(this);
		}
		else if(v == shareShareBtn)
		{
			//type in user to share to
			//add to that user's suggested list
			EditText username = (EditText)sharePopup.getContentView().findViewById(R.id.textAreaShareUser);
			List<User> userList;
			Query query = new Query();
			String userID = "";
			Suggested movie;
			
			if(username.getText().toString() != null || username.getText().toString() != "")
			{
				query.filterOn("name", username.getText().toString());
				userList = UserDataLayer.getUserDataLayer().performQuery(query);
				
				for(int i = 0; i < userList.size(); i++)
				{
					if(username.getText().toString().compareTo(userList.get(i).name) == 0)
					{
						userID = userList.get(i).identifier;
						break;
					}
				}
				
				UserDataLayer.getUserDataLayer().getUserById(userID).suggestedMovies.add(new Suggested(movieID, currentUser.identifier));
				
			}
			
			sharePopup.dismiss();
		}
		else if(v == shareCancelBtn)
		{
			sharePopup.dismiss();
		}
		else if(v == watchBtn)
		{
			//add to list button
			watchPopup = new PopupWindow(inflater.inflate(R.layout.popup_add_to, null, false), 300, 150, true);
			watchPopup.showAtLocation(parent, Gravity.CENTER, 0, 0);
			//add to current user's watch list
			
			if(this.currentUser.moviesToWatch.contains(this.movieID))
			{
				this.currentUser.moviesToWatch.remove(this.movieID);
				watchBtn.setText("Add to List");
			}
			else
			{
				this.currentUser.moviesToWatch.add(this.movieID);
				watchBtn.setText("Remove from List");
			}
			
			watchOkBtn = (Button) watchPopup.getContentView().findViewById(R.id.buttonOk);
			watchOkBtn.setOnClickListener(this);
		}
		else if(v == watchOkBtn)
		{
			watchPopup.dismiss();
		}
	}
}
