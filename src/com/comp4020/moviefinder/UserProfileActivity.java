package com.comp4020.moviefinder;

import android.app.TabActivity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.TabHost;
import android.widget.TextView;
import android.widget.TabHost.TabSpec;

import com.comp4020.moviefinder.data.User;
import com.comp4020.moviefinder.data.UserDataLayer;
import com.comp4020.moviefinder.profile.*;

public class UserProfileActivity extends TabActivity {
	/** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.user_profile);
 
        Intent i = this.getIntent();
        
        Bundle intentBundle = i.getExtras();
        String userID = intentBundle.getString("userID");
        User user = UserDataLayer.getUserDataLayer().getUserById(userID);
        
        TextView txtUser = (TextView)this.findViewById(R.id.textUser);
        TabHost tabHost = getTabHost();
 
        // Username input
        if(user != null)
        {
        	txtUser.setText(user.name);
        }
        
        // Tab for Review
        TabSpec reviewspec = tabHost.newTabSpec("Reviews");
        // setting Title and Icon for the Tab
        reviewspec.setIndicator("Reviews", getResources().getDrawable(R.drawable.icon_review_tab));
        Intent reviewIntent = new Intent(this, ReviewActivity.class);
        reviewIntent.putExtra("userID", user.identifier);
        reviewspec.setContent(reviewIntent);
 
        // Tab for Watch
        TabSpec watchspec = tabHost.newTabSpec("To Watch");
        watchspec.setIndicator("To Watch", getResources().getDrawable(R.drawable.icon_watch_tab));
        Intent watchIntent = new Intent(this, WatchActivity.class);
        watchIntent.putExtra("userID", user.identifier);
        watchspec.setContent(watchIntent);
 
        // Tab for Suggested
        TabSpec suggestedspec = tabHost.newTabSpec("Suggested Films");
        suggestedspec.setIndicator("Suggested Films", getResources().getDrawable(R.drawable.icon_suggested_tab));
        Intent suggestedIntent = new Intent(this, SuggestedActivity.class);
        suggestedIntent.putExtra("userID", user.identifier);
        suggestedspec.setContent(suggestedIntent);
 
        // Adding all TabSpec to TabHost
        tabHost.addTab(reviewspec); // Adding review tab
        tabHost.addTab(watchspec); // Adding watch tab
        tabHost.addTab(suggestedspec); // Adding videos tab
    }
}
