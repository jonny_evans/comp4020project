package com.comp4020.moviefinder.scatterplot;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.PointF;
import android.os.Bundle;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TableLayout.LayoutParams;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;

import com.comp4020.moviefinder.DetailViewActivity;
import com.comp4020.moviefinder.R;
import com.comp4020.moviefinder.data.Movie;
import com.comp4020.moviefinder.data.MovieDataLayer;
import com.comp4020.moviefinder.data.Query;

public class MovieScatterplotActivity extends Activity implements OnClickListener, OnItemClickListener, ScatterplotCanvasDelegate, OnEditorActionListener
{
	ScatterplotCanvas scatterplotView;
	Button filtersButton;
	LinearLayout filterDrawer;
	LinearLayout filterTray;
	TextView filterTrayTitle;
	LinearLayout filterTrayInputLayout;
	ListView filterList;
	List<Movie> movies;
	String[] labels = { "Title", "Genre", "Year", "Length", "Rating" };
	
	FilterArrayAdapter arrayAdapter;
	
	Map<String, String> filters;
	
	public void onCreate(Bundle savedInstanceState) 
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.movie_scatterplot_layout);
        
        this.filters = new HashMap<String, String>();
        
        this.filterDrawer = (LinearLayout)this.findViewById(R.id.filterDrawer);
        this.filterDrawer.setVisibility(View.INVISIBLE);
        
        this.filterList = (ListView)this.findViewById(R.id.filterList);
        this.arrayAdapter = new FilterArrayAdapter(this, labels, this.filters);
        this.filterList.setAdapter(arrayAdapter);
        this.filterList.setOnItemClickListener(this);
        
        //this.filterList.setAdapter(new ArrayAdapter<String>(this, R.layout.list_item, this.labels));
        //this.filterList.setOnItemClickListener(this);
        
        this.filtersButton = (Button)this.findViewById(R.id.filtersButton);
        this.filtersButton.setOnClickListener(this);
        
        this.scatterplotView = (ScatterplotCanvas)this.findViewById(R.id.ScatterplotCanvas);
        this.scatterplotView.delegate = this;
        this.scatterplotView.setXAxisScale(1900, 2010);
        this.scatterplotView.setYAxisScale(0, 10);
        
        this.scatterplotView.zoomView = (ImageView)this.findViewById(R.id.magnifiedView1);
        
        this.filterTray = (LinearLayout)this.findViewById(R.id.filterTray);
        this.filterTrayTitle = (TextView)this.findViewById(R.id.filterTrayTitle);
        this.filterTrayInputLayout = (LinearLayout)this.findViewById(R.id.filterTrayInputLayout);
        
        MovieDataLayer data = MovieDataLayer.getMovieDataLayer();
        
        if(data != null)
        {
            this.movies = data.getAllMovies();
            this.loadMovies(this.filters);
        }   
    }
	
	public void loadMovies(Map<String,String> filters)
	{
		List<PointF> dataPoints = new ArrayList<PointF>();
		List<Movie> movies;
		
		this.arrayAdapter.notifyDataSetChanged();
		
		if(filters.size() > 0)
		{
			Query query = new Query();
			for(String key : filters.keySet())
			{
				query.filterOn(key, filters.get(key));
			}
			movies = MovieDataLayer.getMovieDataLayer().performQuery(query);
		}
		else 
		{
			movies = MovieDataLayer.getMovieDataLayer().getAllMovies();
		}
		
		this.movies = movies;
		        
        for(int i=0; i<movies.size(); i++)
        {
        	Movie movie = movies.get(i);
        	PointF dataPoint = new PointF();
        	dataPoint.x = movie.year;
        	dataPoint.y = movie.rating;
        	
        	dataPoints.add(dataPoint);
        }
                    
        this.scatterplotView.setDataPoints(dataPoints);
	}

	public Movie getMovieForDataPoint(int position)
	{
		return this.movies.get(position);
	}
	
	@Override
	public void onClick(View v) 
	{
		if(this.filterDrawer.getVisibility() == View.VISIBLE)
		{
			this.filtersButton.setText("Show Filters");
			this.filterDrawer.setAnimation(AnimationUtils.loadAnimation(this, android.R.anim.fade_out));
			this.filterDrawer.setVisibility(View.INVISIBLE);
			
			this.dismissKeyboard(this.filtersButton);
			
			if(this.filterTray.getVisibility() == View.VISIBLE)
			{
				this.filterTrayInputLayout.removeAllViews();
				this.filterTray.setAnimation(AnimationUtils.loadAnimation(this, android.R.anim.fade_out));
				this.filterTray.setVisibility(View.INVISIBLE);
			}
		}
		else if(this.filterDrawer.getVisibility() == View.INVISIBLE)
		{
			this.filtersButton.setText("Hide Filters");
			this.filterDrawer.setAnimation(AnimationUtils.loadAnimation(this, android.R.anim.fade_in));
			this.filterDrawer.setVisibility(View.VISIBLE);
		}
	}

	@Override
	public String getTextForDataPointAtIndex(int index) 
	{
		Movie movie = this.movies.get(index);
		return movie.title + " (" + movie.year + ", " + movie.rating + ")";
	}

	@Override
	public void scatterplotDidSelectDataPointAtIndex(int index) 
	{
		Movie movie = this.movies.get(index);
		Intent i = new Intent(this, DetailViewActivity.class);
		i.putExtra("movieID", movie.identifier);
		this.startActivity(i);
	}

	@Override
	public void onItemClick(AdapterView<?> adapter, View view, int position, long id) 
	{
		this.filterTray.setVisibility(View.VISIBLE);
		
		this.filterTrayInputLayout.removeAllViews();
		this.dismissKeyboard(view);
		
		if(position == 0)
		{
			this.filterTrayTitle.setText("Search by Title");
			LayoutParams params = new LayoutParams();
			params.width = LayoutParams.MATCH_PARENT;
			EditText nameField = new EditText(this);
			this.filterTrayInputLayout.addView(nameField, params);
			nameField.setOnEditorActionListener(this);
			
			if(this.filters.get("Title") != null)
			{
				nameField.setText(this.filters.get("Title"));
			}
		}
		else if(position == 1)
		{
			this.filterTrayTitle.setText("Select a Genre");
			
			String[] genres = { "None", "Action", "Adventure", "Drama", "History", "Romance", "War", "Western" };
			
			for(String genre : genres)
			{
				Button button = new Button(this);
				button.setText(genre);
				this.filterTrayInputLayout.addView(button);
				
				final MovieScatterplotActivity activity = this;
				
				if(genre.equals("None"))
				{
					button.setOnClickListener(new OnClickListener() {
						public void onClick(View v) {
							activity.filters.remove("Genre");
							activity.loadMovies(activity.filters);
						}
					});
				}
				else
				{
					button.setOnClickListener(new OnClickListener() {
						public void onClick(View v) {
							activity.filters.put("Genre", ((Button)v).getText().toString());
							activity.loadMovies(activity.filters);
						}
					});
				}
			}
		}
		else if(position == 2)
		{
			this.filterTrayTitle.setText("Filter by Year");
		}
		else if(position == 3)
		{
			this.filterTrayTitle.setText("Filter by Length");
		}
		else if(position == 4)
		{
			this.filterTrayTitle.setText("Filter by Rating");
		}
	}

	@Override
	public boolean onEditorAction(TextView textView, int actionId, KeyEvent event) 
	{
		if(actionId == EditorInfo.IME_NULL && event != null)
		{
			this.filters.put("Title", textView.getText().toString());
			this.loadMovies(this.filters);
			this.dismissKeyboard(textView);
		}
		
		return true;
	}
	
	public void dismissKeyboard(View view)
	{
		InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
		imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
	}
}
