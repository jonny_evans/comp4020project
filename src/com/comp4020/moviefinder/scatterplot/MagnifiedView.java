package com.comp4020.moviefinder.scatterplot;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Paint.Style;
import android.graphics.Path.Direction;
import android.graphics.Region.Op;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.widget.ImageView;

public class MagnifiedView extends ImageView 
{
	Paint borderPaint, targetPaint, bgPaint;
	
	public MagnifiedView(Context context, AttributeSet attrs) 
	{
		super(context, attrs);
		
		this.borderPaint = new Paint();
		this.borderPaint.setColor(Color.GRAY);
		this.borderPaint.setStrokeWidth(5);
		this.borderPaint.setStyle(Style.STROKE);
		this.borderPaint.setAntiAlias(true);
		
		this.targetPaint = new Paint();
		this.targetPaint.setColor(Color.BLACK);
		this.targetPaint.setStrokeWidth(2);
		this.targetPaint.setStyle(Style.STROKE);
		this.targetPaint.setAntiAlias(true);
		
		this.bgPaint = new Paint();
		this.bgPaint.setColor(Color.BLACK);
		this.bgPaint.setStyle(Style.FILL);
		this.bgPaint.setAntiAlias(true);
	}
	
	@Override
	public void onDraw(Canvas canvas)
	{
		Path clipPath = new Path();
	    int w = this.getWidth();
	    int h = this.getHeight();
	    
	    clipPath.addCircle(w/2, h/2, w/2-1, Direction.CW);
	    canvas.clipPath(clipPath);
	    
	    canvas.drawCircle(w/2, h/2, w/2-1, this.bgPaint);
	    
		super.onDraw(canvas);
		
		clipPath = new Path();
		clipPath.addRect(0, 0, w, h, Direction.CW);
		canvas.clipPath(clipPath, Op.REPLACE);
		
		canvas.drawCircle(this.getWidth()/2, this.getHeight()/2, this.getWidth()/2-3, this.borderPaint);
		
		canvas.drawLine(w/2 - 20, h/2, w/2 + 20, h/2, this.targetPaint);
		canvas.drawLine(w/2, h/2 - 20, w/2, h/2 + 20, this.targetPaint);
	}
}
