package com.comp4020.moviefinder.scatterplot;

public interface ScatterplotCanvasDelegate 
{
	public String getTextForDataPointAtIndex(int index);
	public void scatterplotDidSelectDataPointAtIndex(int index);
}
