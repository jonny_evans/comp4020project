package com.comp4020.moviefinder.scatterplot;

import java.util.List;
import java.util.Map;

import com.comp4020.moviefinder.R;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class FilterArrayAdapter extends BaseAdapter
{
	Map<String, String> map;
	String[] values;
	Context context;
	
	public FilterArrayAdapter(Context context, String[] names, Map<String, String> map)
	{
		this.context = context;
		this.values = names;
		this.map = map;
	}
	
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return this.values.length;
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return this.values[position];
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) 
	{
		LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View rowView = inflater.inflate(R.layout.filter_item, parent, false);

		String filterName = (String)this.getItem(position);
		TextView titleView = (TextView)rowView.findViewById(R.id.filter_title);
		titleView.setText(filterName);
		
		Object filterObject = this.map.get(filterName);
		String filterValue = "";
		
		if(filterObject instanceof String)
		{
			filterValue = (String)filterObject;
		}
		
		TextView textView = (TextView)rowView.findViewById(R.id.filter_text);
		textView.setText(filterValue);

		return rowView;
	}
	
}
