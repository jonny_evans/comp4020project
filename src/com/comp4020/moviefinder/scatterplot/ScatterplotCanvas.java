package com.comp4020.moviefinder.scatterplot;

import java.util.List;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PointF;
import android.graphics.Rect;
import android.graphics.Paint.Cap;
import android.graphics.Paint.Style;
import android.graphics.Matrix;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.util.Log;
import android.view.GestureDetector;
import android.view.GestureDetector.OnDoubleTapListener;
import android.view.GestureDetector.OnGestureListener;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.View.OnClickListener;
import android.view.animation.ScaleAnimation;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.Scroller;
import android.widget.TextView;
import android.widget.ImageView.ScaleType;

import com.comp4020.moviefinder.R;

public class ScatterplotCanvas extends View implements OnTouchListener, OnGestureListener, OnDoubleTapListener, OnClickListener
{
	private Paint axisPaint, pointPaint;
	private Rect border;
	private PointF xscale, yscale;
	private float zoom;
	private List<PointF> dataPoints;
	private int selected = -1;
	private int downSelected = -1;
	private int roundRobinSelectionIndex = 0;
	
	private GestureDetector gestureDetector;
	private Scroller scroller;
	private PopupWindow popup;
	private Button popupButton;
	private boolean isShifting;
	
	public ImageView zoomView;
	
	public ScatterplotCanvasDelegate delegate;
	
	public ScatterplotCanvas(Context context, AttributeSet attrs)
	{
		super(context, attrs);
		this.setOnTouchListener(this);
		
		this.zoom = 1.0f;
		
		this.axisPaint = new Paint();
		this.axisPaint.setStrokeWidth(3);
		this.axisPaint.setStrokeCap(Cap.ROUND);
		this.axisPaint.setColor(Color.DKGRAY);
		
		this.pointPaint = new Paint();
		this.pointPaint.setColor(Color.BLUE);
		this.pointPaint.setStyle(Style.FILL);
		this.pointPaint.setStrokeWidth(3);
		this.pointPaint.setAntiAlias(true);
				
		this.border = new Rect(30, 30, 80, 30);
		this.xscale = new PointF(0, 100);
		this.yscale = new PointF(0, 100);
		
		this.gestureDetector = new GestureDetector(this.getContext(), this);
		this.gestureDetector.setOnDoubleTapListener(this);
		this.scroller = new Scroller(this.getContext());
		
		this.popupButton = new Button(this.getContext());
		this.popupButton.setOnClickListener(this);
		
		this.popup = new PopupWindow(popupButton, 180, 100);
		this.popup.setAnimationStyle(R.style.PopupAnimation);
		
		this.isShifting = false;
	}

	public void onDraw(Canvas canvas)
	{
		//canvas area
		int h = this.getHeight();
		int w = this.getWidth();
		
		canvas.scale(this.zoom, this.zoom);
		
		//draw y axis
		canvas.drawLine(this.border.left, h - this.border.bottom, this.border.left, this.border.top, axisPaint);
		
		//draw x axis
		canvas.drawLine(this.border.left, h - this.border.bottom, w - this.border.right, h - this.border.bottom, axisPaint);
		
		if(this.dataPoints != null)
		{
			for(PointF point : this.dataPoints)
			{
				PointF realPoint = this.pointForGraphPoint(point);
				Rect viewport = this.getViewport();
				
				if(viewport.contains((int)realPoint.x, (int)realPoint.y))
				{
					canvas.drawCircle(realPoint.x, realPoint.y, 10, pointPaint);
				}
				else
				{
					Paint paint = new Paint();
					paint.setColor(Color.RED);
					paint.setStyle(Style.STROKE);
					paint.setStrokeWidth(2);
					paint.setAntiAlias(true);
					
					int b = 20;
					
					PointF circleCenter = new PointF();
					circleCenter.x = realPoint.x;
					circleCenter.y = realPoint.y;
					
					if(circleCenter.x < viewport.left)
						circleCenter.x = viewport.left + b;
					if(circleCenter.x > viewport.right)
						circleCenter.x = viewport.right - b;
					if(circleCenter.y < viewport.top)
						circleCenter.y = viewport.top + b;
					if(circleCenter.y > viewport.bottom)
						circleCenter.y = viewport.bottom - b;
					
					Path wedge = new Path();
					wedge.moveTo(realPoint.x, realPoint.y);
					//wedge.lineTo(circleCenter.x, circleCenter.y);

					float slope = (Math.abs(realPoint.y-circleCenter.y) / Math.abs(realPoint.x-circleCenter.x));
					slope = (Float.isInfinite(slope) ? 0 : (1/slope));
					
					if(slope == 0)
					{
						wedge.lineTo(circleCenter.x - 20, circleCenter.y);
						wedge.lineTo(circleCenter.x + 20, circleCenter.y);
						wedge.close();
					}
					else if(Float.isInfinite(slope))
					{
						wedge.lineTo(circleCenter.x, circleCenter.y - 20);
						wedge.lineTo(circleCenter.x, circleCenter.y + 20);
						wedge.close();
					}
					else if(slope < 0)
					{
						wedge.lineTo(circleCenter.x - b, circleCenter.y + (slope * b));
						wedge.lineTo(circleCenter.x + b, circleCenter.y - (slope * b));
						wedge.close();
					}
/*
					else if(slope < 0)
					{
						wedge.lineTo(circleCenter.x + (slope*b), circleCenter.y - (b));
						wedge.lineTo(circleCenter.x - (slope*b), circleCenter.y + (b));
						wedge.close();
					}
					else if(slope > 0)
					{
						wedge.lineTo(circleCenter.x - (slope*b), circleCenter.y - b);
						wedge.lineTo(circleCenter.x + (slope*b), circleCenter.y + b);
						wedge.close();
					}*/
					
					canvas.drawPath(wedge, paint);
					//canvas.drawCircle(circleCenter.x, circleCenter.y, 5, paint);
				}
			}
			
			if(this.selected >= 0)
			{
				Paint selectedPaint = new Paint();
				selectedPaint = new Paint();
				selectedPaint.setColor(Color.GREEN);
				selectedPaint.setStyle(Style.STROKE);
				selectedPaint.setStrokeWidth(3);
				selectedPaint.setAntiAlias(true);
				
				PointF selectedPoint = this.pointForGraphPoint(this.dataPoints.get(this.selected));
				canvas.drawCircle(selectedPoint.x, selectedPoint.y, 15, selectedPaint);
			}
			
			if(this.downSelected >= 0)
			{
				Paint selectedPaint = new Paint();
				selectedPaint = new Paint();
				selectedPaint.setColor(Color.RED);
				selectedPaint.setStyle(Style.STROKE);
				selectedPaint.setStrokeWidth(3);
				selectedPaint.setAntiAlias(true);				
				
				PointF selectedPoint = this.pointForGraphPoint(this.dataPoints.get(this.downSelected));
				canvas.drawCircle(selectedPoint.x, selectedPoint.y, 15, selectedPaint);
			}
		}
		
		if(this.scroller.computeScrollOffset())
		{
			this.scrollTo(this.scroller.getCurrX(), this.scroller.getCurrY());
		}
	}
	
	@Override
	public boolean onTouch(View v, MotionEvent event) 
	{	
		if(this.isShifting)
		{
			if(event.getAction() == MotionEvent.ACTION_MOVE)
			{
				PointF size = new PointF(this.zoomView.getWidth(), this.zoomView.getHeight());
				
				Matrix matrix = new Matrix();
				matrix.postScale(1.0f, 1.0f);
				matrix.postTranslate(-event.getX() + size.x/2, -event.getY() + size.y/2);
								
				this.zoomView.layout((int)(event.getX() - size.x/2), (int)(event.getY() - size.y/2 - size.y), 
						(int)(event.getX() + size.x/2), (int)(event.getY() + size.y/2 - size.y));
				
				this.zoomView.setVisibility(View.VISIBLE);
				this.zoomView.setImageMatrix(matrix);
				this.zoomView.setScaleType(ScaleType.MATRIX);
				this.zoomView.invalidate();
				
				//this.downSelected = this.dataPointIndexForPoint(new PointF(event.getX(), event.getY()), 20);
				//this.invalidate();
			}
			else if(event.getAction() == MotionEvent.ACTION_UP)
			{
				this.isShifting = false;
				this.zoomView.setImageBitmap(null);
				this.zoomView.setVisibility(View.INVISIBLE);
				this.selectMovieAtPoint(new PointF(event.getX(), event.getY()));
			}
			
			return true;
		}
		else if(event.getAction() == MotionEvent.ACTION_UP)
		{
			this.downSelected = -1;
			this.invalidate();
		}
		return false;
	}
	
	public boolean onTouchEvent(MotionEvent event)
	{
		return this.gestureDetector.onTouchEvent(event);
	}
	
	// Convenience Methods
	
	public PointF pointForGraphPoint(PointF p)
	{
		float scaleX = this.xscale.y - this.xscale.x;
		float scaleY = this.yscale.y - this.yscale.x;
		
		float sizeX = this.getWidth() - this.border.left - this.border.right;
		float sizeY = this.getHeight() - this.border.top - this.border.bottom;
		
		float unitsPerPxX = sizeX / scaleX;
		float unitsPerPxY = sizeY / scaleY;
		
		PointF retval = new PointF();
		retval.x = unitsPerPxX * (p.x - this.xscale.x) + this.border.left;
		retval.y = this.border.top + (unitsPerPxY * this.yscale.y) - (unitsPerPxY * p.y);
		
		return retval;
	}
	
	public int dataPointIndexForPoint(PointF point)
	{
		return this.dataPointIndexForPoint(point, 44);
	}
	
	public int dataPointIndexForPoint(PointF point, int buffer)
	{
		int retval = -1;
		
		PointF touchedPoint = new PointF(point.x, point.y);
		touchedPoint.x += this.getScrollX();
		touchedPoint.y += this.getScrollY();
		touchedPoint.x *= 1 / this.zoom;
		touchedPoint.y *= 1 / this.zoom;
		
		for(int i=0; i<this.dataPoints.size(); i++)
		{
			int index = (i+this.roundRobinSelectionIndex)%this.dataPoints.size();
			PointF dataPoint = this.dataPoints.get(index);
			PointF absPoint = this.pointForGraphPoint(dataPoint);
			
			float diffX = Math.abs(touchedPoint.x - absPoint.x);
			float diffY = Math.abs(touchedPoint.y - absPoint.y);
			
			if(diffX < buffer && diffY < buffer)
			{
				retval = this.dataPoints.indexOf(dataPoint);
				this.roundRobinSelectionIndex = (retval+1)%this.dataPoints.size();
				break;
			}
		}

		return retval;
	}
	
	// Properties
	
	public void setXAxisScale(float start, float end)
	{
		xscale.x = start;
		xscale.y = end;
		this.invalidate();
	}
	
	public void setYAxisScale(float start, float end)
	{
		yscale.x = start;
		yscale.y = end;
		this.invalidate();
	}
	
	public void setDataPoints(List<PointF> points)
	{
		this.dataPoints = points;
		this.invalidate();
	}
	
	// GestureDetector Methods
	
	public boolean onSingleTapConfirmed(MotionEvent e) 
	{
		PointF touchedPoint = new PointF(e.getX(), e.getY());		
		this.selectMovieAtPoint(touchedPoint);
		return true;
	}
	
	public void selectMovieAtPoint(PointF point)
	{
		if(this.popup.isShowing())
			this.popup.dismiss();
		this.selected = this.dataPointIndexForPoint(point);
		
		if(this.selected >= 0 && this.delegate != null)
		{
			String text = this.delegate.getTextForDataPointAtIndex(this.selected);
			
			if(text != null)
			{
				PointF onScreenPoint = this.pointForGraphPoint(this.dataPoints.get(this.selected));
				onScreenPoint.x /= 1 / this.zoom;
				onScreenPoint.y /= 1 / this.zoom;
				onScreenPoint.x -= this.getScrollX();
				onScreenPoint.y -= this.getScrollY();
				
				popupButton.setText(text);
				// TODO: Detect what side the popup should appear on, this is determined by how close to the edges of the screen we are
				popup.showAtLocation(this, Gravity.NO_GRAVITY, (int)onScreenPoint.x + 10, (int)onScreenPoint.y + 10);
			}
		}
		
		this.invalidate();
	}

	@Override
	public void onClick(View v) 
	{
		if(this.delegate != null)
		{
			this.delegate.scatterplotDidSelectDataPointAtIndex(this.selected);
		}
	}
	
	@Override
	public boolean onDoubleTap(MotionEvent e) 
	{
		//ScaleAnimation scaleAnimation;
		
		if(this.popup.isShowing())
			this.popup.dismiss();
		
		if(this.zoom == 1.0f)
		{
			//scaleAnimation = new ScaleAnimation(1.0f, 1.5f, 1.0f, 1.5f, e.getX(), e.getY());
			this.zoom = 1.5f;
		}
		else
		{
			//scaleAnimation = new ScaleAnimation(1.5f, 1.0f, 1.5f, 1.0f, e.getX(), e.getY());
			this.zoom = 1.0f;
			this.scrollTo(0, 0);
		}
		
		this.invalidate();
		
		//scaleAnimation.setDuration(500);
		//scaleAnimation.setFillEnabled(true);
		//scaleAnimation.setFillAfter(true);
		//this.startAnimation(scaleAnimation);
		
		return true;
	}
	
	@Override
	public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) 
	{
		if(this.zoom != 1)
		{
			if(this.popup.isShowing())
				this.popup.dismiss();
			this.downSelected = -1;
			
			this.scrollBy((int)distanceX, (int)distanceY);
			
			//Log.d("MovieFinder", "X:" + this.getScrollX() + " Y:" + this.getScrollY());
			
			return true;
		}
		else
		{
			return false;
		}
	}
	
	@Override
	public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) 
	{
		PointF bounds;
		
		if(this.zoom == 1)
			bounds = new PointF(0,0);
		else
			bounds = new PointF(this.getWidth()*(1/this.zoom), this.getHeight()*(1/this.zoom));
		
		this.scroller.fling(this.getScrollX(), this.getScrollY(), -(int)velocityX, -(int)velocityY, 0, (int)bounds.x, 0, (int)bounds.y);
		this.invalidate();
		return true;
	}
	
	@Override
	public boolean onDown(MotionEvent e) 
	{
		PointF touchedPoint = new PointF(e.getX(), e.getY());
		this.downSelected = this.dataPointIndexForPoint(touchedPoint);
		this.invalidate();
		
		if(this.scroller.isFinished() == false)
		{
			this.scroller.forceFinished(true);
		}
		return true;
	}

	@Override
	public void onLongPress(MotionEvent e) 
	{
		if(this.zoom == 1)
		{
			this.isShifting = true;
			Bitmap bitmap = Bitmap.createBitmap(this.getWidth(), this.getHeight(), Config.ARGB_8888);
			Canvas canvas = new Canvas(bitmap);
			this.draw(canvas);
			this.zoomView.setImageBitmap(bitmap);
		}
		
		this.downSelected = -1;
		this.invalidate();
	}

	public Rect getViewport()
	{
		Rect retval = new Rect();
		retval.top = (int)(this.getScrollY() * (1/this.zoom));
		retval.left = (int)(this.getScrollX() * (1/this.zoom));
		retval.right = (int)(retval.left + (this.getWidth() * (1/this.zoom)));
		retval.bottom = (int)(retval.top + (this.getHeight() * (1/this.zoom)));
		
		return retval;
	}
	
	@Override
	public void onShowPress(MotionEvent e) {
		// TODO Auto-generated method stub
	}

	@Override
	public boolean onSingleTapUp(MotionEvent arg0) 
	{
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean onDoubleTapEvent(MotionEvent arg0) 
	{
		// TODO Auto-generated method stub
		return false;
	}
}
