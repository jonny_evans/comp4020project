package com.comp4020.moviefinder;

import com.comp4020.moviefinder.data.MovieDataLayer;
import com.comp4020.moviefinder.data.User;
import com.comp4020.moviefinder.data.UserDataLayer;
import com.comp4020.moviefinder.scatterplot.MovieScatterplotActivity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageButton;

public class MovieFinderActivity extends Activity implements OnClickListener 
{
	private ImageButton detailsButton, profileButton, scatterplotButton;
	
    public void onCreate(Bundle savedInstanceState) 
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        
        //detailsButton = (Button)this.findViewById(R.id.DetailsButton);
        //detailsButton.setOnClickListener(this);
        
        profileButton = (ImageButton)this.findViewById(R.id.ProfileButton);
        profileButton.setOnClickListener(this);
        
        scatterplotButton = (ImageButton)this.findViewById(R.id.ScatterplotButton);
        scatterplotButton.setOnClickListener(this);
        
        if(MovieDataLayer.getMovieDataLayer() == null)
        {
            MovieDataLayer data = new MovieDataLayer(this.getResources());
            data.loadMoviesFromXML("movies");
        }
    }
    
	public void onClick(View v) 
	{
		Intent i = null;
		
		if(v == this.detailsButton)
		{
			i = new Intent(this, DetailViewActivity.class);
			i.putExtra("movieID", "5");
		}
		else if (v == this.profileButton)
		{
			i = new Intent(this, UserProfileActivity.class);
			i.putExtra("userID", "0");
		}
		else if(v == this.scatterplotButton)
		{
			i = new Intent(this, MovieScatterplotActivity.class);
		}
		
		if(i != null)
			startActivity(i);
	}
}