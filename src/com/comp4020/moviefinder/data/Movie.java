package com.comp4020.moviefinder.data;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Movie 
{
	private static int globalID = 0;
	
	public String identifier;			// Should generate this on creation
	public String title; 				// Required
	public int year;					// Required
	public int length;					// Required
	public String certification; 		// Not present in all (optional)
	public int rating;					// Required
	public List<String> genres;			// Can have more than one
	public List<String> actors;			// Can have more than one
	public String director;				// May have more than one? Assumed only one for now
	public List<String> reviewers;		// Contains userid of anyone that reviewed the movie (ugh, this is an awful hack...)
	
	public Movie()
	{
		this.identifier = String.valueOf(Movie.globalID++);
		this.genres = new ArrayList<String>();
		this.actors = new ArrayList<String>();
		this.reviewers = new ArrayList<String>();
	}
	
	public String getIdentifier() { return this.identifier; }
	public String getTitle() { return this.title; }
	public int getYear() { return this.year; }
	public int getLength() { return this.length; }
	public String getCertification() { return this.certification; }
	public int getRating() { return this.rating; }
	public String getDirector() { return this.getDirector(); }
	
	public String getGenre()
	{
		StringBuilder strBld = new StringBuilder(this.genres.toString());

		strBld.deleteCharAt(0);
		strBld.deleteCharAt(strBld.length()-1);
		
		return strBld.toString();
	}
	
	public String getActors()
	{
		StringBuilder strBld = new StringBuilder(this.actors.toString());

		strBld.deleteCharAt(0);
		strBld.deleteCharAt(strBld.length()-1);
		
		return strBld.toString();
	}
}
