package com.comp4020.moviefinder.data;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class UserDataLayer 
{
	private static UserDataLayer userDataLayer = null;
	private Map<String, User> users;
	
	public static UserDataLayer getUserDataLayer()
	{
		if(UserDataLayer.userDataLayer == null)
		{
			UserDataLayer.userDataLayer = new UserDataLayer();
		}
		
		return UserDataLayer.userDataLayer;
	}
	
	public UserDataLayer()
	{
		this.users = new HashMap<String, User>();
		
		User user = new User();
		user.name = "Johan McBerkenstein";
		
		Review review = new Review("0", "This is my review!", 5);
		user.reviews.put(review.movieID, review);
		
		user.moviesToWatch.add("0");
		user.moviesToWatch.add("2");
		user.moviesToWatch.add("4");
		
		Suggested suggest = new Suggested("6","1");
		user.suggestedMovies.add(suggest);
		
		this.users.put(user.identifier, user);
		
		user = new User();
		user.name = "Laggy McLaggington";
		
		review = new Review("6", "This movie was totally awesome! The director has made a masterpiece with this one! I highly recommend you watch this!", 10);
		user.reviews.put(review.movieID, review);
		
		user.moviesToWatch.add("6");
		user.moviesToWatch.add("5");
		user.moviesToWatch.add("3");
		
		suggest = new Suggested("2","0");
		user.suggestedMovies.add(suggest);
		
		this.users.put(user.identifier, user);
	}
	
	public List<User> performQuery(Query query)
	{
		List<User> retval = new ArrayList<User>();
		Map<String, String> containsQueries = query.getContainsQueries();
		boolean accepted = false;
		User[] users = this.users.values().toArray(new User[0]);
		
		for(User user : users)
		{			
			try
			{
				accepted = true;

				for(String key : containsQueries.keySet())
				{
					String methodName = "get" + key.substring(0, 1).toUpperCase() + key.substring(1);
					Method method = User.class.getMethod(methodName);
					Object returnValue = method.invoke(user);
					Object compareTo = containsQueries.get(key);
					
					if(returnValue.getClass().equals(compareTo.getClass()))
					{
						if(returnValue instanceof String)
						{
							accepted = ((String) returnValue).contains((String)compareTo);
						}
						else
						{
							accepted = returnValue.equals(compareTo);
						}
					}
					if(!accepted)
						break;
				}
				
				if(accepted)
					retval.add(user);
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
		}
		
		return retval;	
	}
	
	public User getCurrentUser()
	{
		return this.users.get("0");
	}
	
	public User[] getAllUsers()
	{
		User[] userArray = new User[0];
		return this.users.values().toArray(userArray);
	}
	
	public User getUserById(String userId)
	{
		return this.users.get(userId);
	}
}
