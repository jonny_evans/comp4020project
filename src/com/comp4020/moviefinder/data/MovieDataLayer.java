package com.comp4020.moviefinder.data;

import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import android.content.res.Resources;
import android.util.Log;

public class MovieDataLayer 
{
	private Resources resources = null;
	private Movie currentMovie = null;
	private List<Movie> movieList = null;
	private String currentTagName = null;
	
	private static MovieDataLayer datalayer = null;
	
	public static MovieDataLayer getMovieDataLayer()
	{
		return MovieDataLayer.datalayer;
	}
	
	public MovieDataLayer(Resources resources)
	{
		super();
		
		this.resources = resources;
		this.movieList = new ArrayList<Movie>();
		
		MovieDataLayer.datalayer = this;
	}
	
	public List<Movie> getAllMovies()
	{
		return this.movieList;
	}
	
	public Movie getMovieById(String movieId)
	{
		Movie retval = null;
		
		for(Movie movie : this.movieList)
		{
			if(movie.identifier.equalsIgnoreCase(movieId))
			{
				retval = movie;
				break;
			}
		}
		
		return retval;
	}
	
	public List<Movie> performQuery(Query query)
	{
		List<Movie> retval = new ArrayList<Movie>();
		Map<String, String> containsQueries = query.getContainsQueries();
		boolean accepted = false;
				
		for(Movie movie : this.movieList)
		{			
			try
			{
				accepted = true;

				for(String key : containsQueries.keySet())
				{
					String methodName = "get" + key.substring(0, 1).toUpperCase() + key.substring(1);
					Method method = Movie.class.getMethod(methodName);
					Object returnValue = method.invoke(movie);
					Object compareTo = containsQueries.get(key);
					
					if(returnValue.getClass().equals(compareTo.getClass()))
					{
						if(returnValue instanceof String)
						{
							returnValue = ((String) returnValue).toLowerCase();
							compareTo = ((String)compareTo).toLowerCase();
							accepted = ((String) returnValue).contains((String)compareTo);
						}
						else
						{
							accepted = returnValue.equals(compareTo);
						}
					}
					if(!accepted)
						break;
				}
				
				if(accepted)
					retval.add(movie);
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
		}
		
		return retval;
	}
	
	public void loadMoviesFromXML(String fileToLoad)
	{		
		try 
		{
			int fileID = resources.getIdentifier("com.comp4020.moviefinder:raw/"+fileToLoad, null, null);
			InputStream istr = this.resources.openRawResource(fileID);
			XmlPullParserFactory factory = XmlPullParserFactory.newInstance(); 
	        factory.setNamespaceAware(true);
	       
            XmlPullParser xrp = factory.newPullParser(); 
			xrp.setInput(istr, "UTF-8");
			
			int eventType = xrp.getEventType();
			long startTime = System.currentTimeMillis();

			do
			{
				switch(eventType)
				{
				case XmlPullParser.START_DOCUMENT:
					break;
				case XmlPullParser.START_TAG:
					this.processStartElement(xrp);
					break;
				case XmlPullParser.END_TAG:
					this.processEndElement(xrp);
					break;
				case XmlPullParser.TEXT:
					this.processText(xrp);
					break;
				}
				
				eventType = xrp.next();
			}
			while(eventType != XmlPullParser.END_DOCUMENT);
			
			this.processEndDocument(xrp);
			
			long elapsedTime = System.currentTimeMillis() - startTime;
			
			Log.d("MovieFinder", "Imported " + this.movieList.size() + " movies in " + elapsedTime + "ms");
		} 
		catch (XmlPullParserException e) 
		{
			e.printStackTrace();
		}
		catch(IOException ioe)
		{
			ioe.printStackTrace();
		}
		
		// TODO: We should probably only do the import from XML once then dump them into an SQLite database with the movies.
		// This'll let us load up faster (the XML import takes an entire 10 seconds for 89 movies right now) and will let
		// us perform SQL queries on the data instead of writing code that manually searches a list.
	}
	
	// ******************************************************************************************
	// XML Parsing Methods
	//
	// These are called automatically by the parser when it encounters a tag
	// ******************************************************************************************
	
	public void processStartElement(XmlPullParser xrp)
	{
		String startTagText = xrp.getName();
		
		if(startTagText.equalsIgnoreCase("movie"))
		{
			this.currentMovie = new Movie();
		}
				
		this.currentTagName = startTagText;
	}
	
	public void processText(XmlPullParser xrp)
	{
		String tagName = this.currentTagName;
		String tagText = xrp.getText();
				
		if(tagName != null)
		{
			if(tagName.equals("title"))
				this.currentMovie.title = tagText;
			else if(tagName.equals("year"))
				this.currentMovie.year = Integer.parseInt(tagText);
			else if(tagName.equals("rating"))
				this.currentMovie.rating = Integer.parseInt(tagText);
			else if(tagName.equals("length"))
				this.currentMovie.length = Integer.parseInt(tagText.substring(0, tagText.indexOf(" ")));
			else if(tagName.equals("certification"))
				this.currentMovie.certification = tagText;
			else if(tagName.equals("director"))
				this.currentMovie.director = tagText;
			else if(tagName.equals("genre"))
				this.currentMovie.genres.add(tagText);
			else if(tagName.equals("actor"))
				this.currentMovie.actors.add(tagText);
		}
	}
	
	public void processEndElement(XmlPullParser xrp)
	{
		String endTagText = xrp.getName();
		
		this.currentTagName = null;
		
		if(endTagText.equalsIgnoreCase("movie"))
		{
			this.movieList.add(this.currentMovie);
			this.currentMovie = null;
		}
	}
	
	public void processEndDocument(XmlPullParser xrp)
	{
		
	}
}
