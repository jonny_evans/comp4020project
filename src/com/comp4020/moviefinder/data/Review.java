package com.comp4020.moviefinder.data;

public class Review 
{
	public String movieID;
	public String review;
	public int rating;
	
	public Review()
	{
		this.movieID = "";
		this.review = "";
		this.rating = 0;
	}
	
	public Review(String movieId, String review, int rating)
	{
		this.movieID = movieId;
		this.review = review;
		this.rating = rating;
	}
}
