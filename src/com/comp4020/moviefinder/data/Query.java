package com.comp4020.moviefinder.data;

import java.util.HashMap;
import java.util.Map;

public class Query 
{
	private Map<String, String> containsQueries;
	
	public Query()
	{
		super();
	}
	
	public void filterOn(String key, String value)
	{
		if(this.containsQueries == null)
			this.containsQueries = new HashMap<String, String>();
		this.containsQueries.put(key, value);
	}
	
	public void filterBetween(String key, int start, int end)
	{
		
	}
	
	public Map<String, String> getContainsQueries()
	{
		return this.containsQueries;
	}
}
