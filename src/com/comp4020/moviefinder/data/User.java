package com.comp4020.moviefinder.data;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class User 
{
	private static int globalID = 0;
	
	public String identifier;
	public String name;
	public Map<String, Review> reviews;
	public List<String> moviesToWatch;
	public List<Suggested> suggestedMovies;
	
	public User()
	{
		this.identifier = String.valueOf(User.globalID++);
		this.name = "Unnamed";
		this.reviews = new HashMap<String, Review>();
   		this.moviesToWatch = new ArrayList<String>();
   		this.suggestedMovies = new ArrayList<Suggested>();
	}
	
	public String getIdentifier() { return this.identifier; }
	public String getName() { return this.name; }
	public Map<String, Review> getReviews() { return this.reviews; }
	public List<String> getMoviesToWatch() { return this.moviesToWatch; }
	public List<Suggested> getSuggestedMovies() { return this.suggestedMovies; }
}
