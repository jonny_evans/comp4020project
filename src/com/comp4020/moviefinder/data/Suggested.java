package com.comp4020.moviefinder.data;

public class Suggested
{
	public String movieID;
	public String userID;
	
	public Suggested(String movieId, String userID)
	{
		this.movieID = movieId;
		this.userID = userID;
	}
}
